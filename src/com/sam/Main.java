package com.sam;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String vertext4 = "4vertex.txt";
        String vertext8 = "8vertex.txt";
        String vertext10 = "10vertex.txt";
        String vertext11 = "11vertex.txt";
        String vertext12 = "12vertex.txt";
        String vertext13 = "13vertex.txt";
        int[][] graph = initialGraph(vertext12);
        int[] color = new int[graph.length];
        System.out.println("Number of vertex: "+ graph.length);
        System.out.println("Start graph coloring with backtracking: " + java.time.LocalDateTime.now());
        BackTrackingAlgorithm backTrackingAlgorithm= new BackTrackingAlgorithm(graph.length, color);
        backTrackingAlgorithm.graphColoring(graph);
    }

    private static int[][] initialGraph(String filePath){
        try {
            File file = new File(filePath);
            BufferedReader br = null;
            br = new BufferedReader(new FileReader(file));
            String st;
            int n = 0;
            int[][] graph = new int[100][100];
            while (((st = br.readLine()) != null)) {
                String tmp[] = st.split(" ");
                for (int j = 0; j < tmp.length; j++) {
                    graph[n][j] = Integer.parseInt(tmp[j]);
                }
                n++;
            }
            int[][] graphColoring = new int[n][n];
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    graphColoring[i][j] = graph[i][j];
                }
            }
            System.out.println("---***---GraphColor---***---");
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    System.out.print(graphColoring[i][j] + "\t");
                }
                System.out.println("\n");
            }
            return graphColoring;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }
}
