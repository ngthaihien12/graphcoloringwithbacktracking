package com.sam;

import java.util.ArrayList;
import java.util.List;

public class Permute {
    ArrayList<Integer> temp = new ArrayList<>();
    private CallBack callBack;

    public void permute(int[] nums) {
        List<Integer> result = new ArrayList<>();
        dfs(nums, result);
    }

    public void dfs(int[] nums, List<Integer> result) {
        if (nums.length == result.size()) {
            temp.addAll(result);
            callBack.onDataCallBack(temp);
            temp.clear();
        }
        for (int i=0; i<nums.length; i++) {
            if (!result.contains(nums[i])) {
                result.add(nums[i]);
                dfs(nums, result);
                result.remove(result.size() - 1);
            }
        }
    }

    public interface CallBack{
        void onDataCallBack(List<Integer> temp);
    }

    public void setOnDataCallBack(CallBack callBack){
        this.callBack= callBack;
    }
}
