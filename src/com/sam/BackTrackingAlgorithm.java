package com.sam;

import java.util.List;

public class BackTrackingAlgorithm {
    int V;
    int color[];
    int bestResult;
    int bestColor[];
    //    int bestColorIndex;
    List<Integer> current;
    List<Integer> bestColorIndex ;
    List<List<Integer>> listResultPermute;

    public BackTrackingAlgorithm(int v, int[] color) {
        V = v;
        this.color = color;
    }

    /* A utility function to check if the current
           color assignment is safe for vertex v */
    private boolean isSafe(int v, int graph[][], int color[],
                           int c) {
        for (int i = 0; i < V; i++)
            if (graph[v][i] == 1 && c == color[i])
                return false;
        return true;
    }

    /* A recursive utility function to solve m
       coloring  problem */
    private boolean graphColoringUtil(int graph[][],
                                      int color[], int v) {
        /* base case: If all vertices are assigned
           a color then return true */
        if (v == V) {
            for (int i=0; i< color.length; i++){
                if(color[i]==0){
                    graphColoringUtil(graph, color, i);
                }
            }
            if(bestResult > countColor(color, V) ){
                bestResult = countColor(color, V);
                bestColor = color;
                bestColorIndex= current;
            }
            return true;
        }

        /* Consider this vertex v and try different
           colors */
        for (int c = 1; c <= V; c++) {
            /* Check if assignment of color c to v
               is fine*/
            if (isSafe(v, graph, color, c)) {
                if (color[v] == 0) {
                    color[v] = c;
                }

                /* recur to assign colors to rest
                   of the vertices */
                if (graphColoringUtil(graph,
                        color, v + 1))
                    return true;

                /* If assigning color c doesn't lead
                   to a solution then remove it */
                color[v] = 0;
            }
        }

        /* If no color can be assigned to this vertex
           then return false */
        return false;
    }

    /* This function solves the m Coloring problem using
       Backtracking. It mainly uses graphColoringUtil()
       to solve the problem. It returns false if the m
       colors cannot be assigned, otherwise return true
       and  prints assignments of colors to all vertices.
       Please note that there  may be more than one
       solutions, this function prints one of the
       feasible solutions.*/
    public int[] graphColoring(int graph[][]) {
        // Call graphColoringUtil() for vertex 0
        Permute p = new Permute();
        int[] num = new int[graph.length];
        for (int i = 0; i < graph.length; i++){
            num[i] = i + 1;
        }
        p.setOnDataCallBack(new Permute.CallBack() {
            @Override
            public void onDataCallBack(List<Integer> temp) {
                current = temp;
                graphColoringUtil(graph, color, current.get(0));
            }
        });
        p.permute(num);

        // Print the solution
        if (bestColor != null){
            printSolution(bestColor);
            return bestColor;
        } else {
            printSolution(color);
            return color;
        }
    }

    /* A utility function to print solution */
    private void printSolution(int color[]) {
        System.out.println("Graph coloring finish: " + java.time.LocalDateTime.now());
        System.out.println("Following are the assigned colors");
        for (int i = 0; i < color.length; i++)
            System.out.print(" " + color[i] + " ");
        System.out.println();
        System.out.println("Number of color: "+ countColor(color, color.length));
    }

    private int countColor(int[] color, int n) {
        int res = 1;

        // Pick all elements one by one
        for (int i = 1; i < n; i++)
        {
            int j =0;
            for (j = 0; j < i; j++)
                if (color[i] == color[j])
                    break;

            // If not printed earlier,
            // then print it
            if (i == j)
                res++;
        }
        return res;
    }
}
